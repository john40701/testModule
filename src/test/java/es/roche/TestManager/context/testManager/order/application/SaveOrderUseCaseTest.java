package es.roche.TestManager.context.testManager.order.application;

import es.roche.TestManager.context.testManager.order.domain.dto.TestDTO;
import es.roche.TestManager.context.testManager.order.domain.service.SaveOrderService;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(classes = {SaveOrderUseCase.class, SaveOrderService.class})
class SaveOrderUseCaseTest {

    @Autowired
    private SaveOrderUseCase useCase;

    @Test
    public void saveOrder(){
        String testA = "Test User A";
        List<TestDTO> tests = Lists.newArrayList(
                new TestDTO("Glucose", "Param A Glucose"),
                new TestDTO("Hemoglobin", "Param A Hemoglobin"),
                new TestDTO("Sodio", "Param A Sodio")
        );
        Boolean success = useCase.execute(testA,tests );
        assertTrue(success);

    }

    @Test
    public void saveOrderWrong(){
        String testA = "Test User A";
        List<TestDTO> tests = Lists.newArrayList(
                new TestDTO("GlucoseV2", "Param A Glucose"),
                new TestDTO("Hemoglobin", "Param A Hemoglobin"),
                new TestDTO("Sodio", "Param A Sodio")
        );
        Boolean success = useCase.execute(testA,tests );
        assertFalse(success);

    }

}