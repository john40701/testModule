package es.roche.TestManager.context.testManager.order.application;

import es.roche.TestManager.context.testManager.order.domain.service.SaveOrderService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = {OperationExecutorUseCase.class})
class OperationExecutorUseCaseTest {

    @Autowired
    private OperationExecutorUseCase useCase;

    @Test
    public void glucoseOperationA() throws Exception {
        useCase.execute("Glucose", "OperationA", "paramA");
    }

    @Test
    public void glucoseOperationB() throws Exception {
        useCase.execute("Glucose", "OperationB", "paramA");
    }

    @Test
    public void glucoseBiochemistry() throws Exception {
        useCase.execute("Glucose", "Biochemistry", "paramA");
    }

    @Test
    public void sodioOperationA() throws Exception {
        useCase.execute("Sodio", "OperationA", "paramA");
    }

}