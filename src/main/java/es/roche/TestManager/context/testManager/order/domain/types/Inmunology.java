package es.roche.TestManager.context.testManager.order.domain.types;

public class Inmunology extends Test {

    protected String propB;

    public Inmunology(String propA, String propB) {
        super(propA);
        this.propB = propB;
    }

    public void InmunologyOperation() {
        System.out.println("Generic Operation A, parameter:" + propA);
        propA += " Validated";
        propB += " Validated";
    }
}
