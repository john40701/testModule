package es.roche.TestManager.context.testManager.order.domain.commads;

import es.roche.TestManager.context.testManager.order.domain.types.Biochemistry;

public class BiochemistryOperationA implements ExamOperation{

    private Biochemistry biochemistry;

    public BiochemistryOperationA(Biochemistry biochemistry) {
        this.biochemistry = biochemistry;
    }

    @Override
    public void excecute() {
        biochemistry.biochemistryOperationA();
    }
}
