package es.roche.TestManager.context.testManager.order.domain.commads;

import es.roche.TestManager.context.testManager.order.domain.types.Test;

public class GenericOperationB implements ExamOperation {

    private Test test;

    public GenericOperationB(Test test) {
        this.test = test;
    }

    @Override
    public void excecute() {
        test.genericOperationB();
    }
}
