package es.roche.TestManager.context.testManager.order.domain;

import es.roche.TestManager.context.testManager.order.domain.types.Hematology;

final public class Hemoglobin extends Hematology {
    public Hemoglobin(String propA) {
        super(propA);
    }
}
