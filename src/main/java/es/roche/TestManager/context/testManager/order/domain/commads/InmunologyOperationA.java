package es.roche.TestManager.context.testManager.order.domain.commads;

import es.roche.TestManager.context.testManager.order.domain.types.Inmunology;

public class InmunologyOperationA implements ExamOperation{

    private Inmunology inmunology;

    public InmunologyOperationA(Inmunology inmunology) {
        this.inmunology = inmunology;
    }

    @Override
    public void excecute() {
        inmunology.InmunologyOperation();
    }
}
