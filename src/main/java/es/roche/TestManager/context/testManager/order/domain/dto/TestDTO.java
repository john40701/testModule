package es.roche.TestManager.context.testManager.order.domain.dto;

public class TestDTO {

    public String name;

    public String paramA;

    public String paramB;

    public TestDTO(String name, String paramA, String paramB) {
        this.name = name;
        this.paramA = paramA;
        this.paramB = paramB;
    }

    public TestDTO(String name, String paramA) {
        this.name = name;
        this.paramA = paramA;
    }

    public String getName() {
        return name;
    }

    public String getParamA() {
        return paramA;
    }

    public String getParamB() {
        return paramB;
    }
}
