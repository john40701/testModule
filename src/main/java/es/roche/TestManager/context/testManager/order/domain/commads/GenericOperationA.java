package es.roche.TestManager.context.testManager.order.domain.commads;

import es.roche.TestManager.context.testManager.order.domain.types.Test;

public class GenericOperationA implements ExamOperation{

    private Test test;

    public GenericOperationA(Test test) {
        this.test = test;
    }

    @Override
    public void excecute() {
        test.genericOperationA();
    }
}
