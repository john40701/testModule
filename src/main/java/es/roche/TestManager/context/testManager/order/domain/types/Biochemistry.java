package es.roche.TestManager.context.testManager.order.domain.types;

public abstract class Biochemistry extends Test {

    public Biochemistry(String propA) {
        super(propA);
    }

    public String biochemistryOperationA(){
        propA += " Validated";
        System.out.println("Generic Operation A, parameter:" + propA);
        return propA;
    }

}
