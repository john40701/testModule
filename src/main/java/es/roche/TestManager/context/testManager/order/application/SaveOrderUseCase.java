package es.roche.TestManager.context.testManager.order.application;

import es.roche.TestManager.context.testManager.order.domain.dto.TestDTO;
import es.roche.TestManager.context.testManager.order.domain.service.SaveOrderService;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SaveOrderUseCase {

    final private SaveOrderService service;

    public SaveOrderUseCase(SaveOrderService service) {
        this.service = service;
    }

    public boolean execute(String paramA, List<TestDTO> test){
        return service.save(paramA,test);
    }
}
