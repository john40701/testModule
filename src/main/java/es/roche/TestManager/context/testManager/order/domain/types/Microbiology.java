package es.roche.TestManager.context.testManager.order.domain.types;

public abstract class Microbiology extends Test {

    public Microbiology(String propA) {
        super(propA);
    }

    @Override
    public void genericOperationA(){
        System.out.println("Override Operation A, parameter:" + propA);
    }

}
