package es.roche.TestManager.context.testManager.order.domain;

import es.roche.TestManager.context.testManager.order.domain.commads.BiochemistryOperationA;
import es.roche.TestManager.context.testManager.order.domain.commads.ExamOperation;
import es.roche.TestManager.context.testManager.order.domain.commads.GenericOperationA;
import es.roche.TestManager.context.testManager.order.domain.commads.GenericOperationB;
import es.roche.TestManager.context.testManager.order.domain.types.Biochemistry;
import es.roche.TestManager.context.testManager.order.domain.types.Test;
import org.apache.commons.lang3.ArrayUtils;

public class Command {

    private String examName;

    private String operation;

    private String propA;

    private String propB;

    private Test test;

    public Command(String examName, String operation, String... args) throws Exception {
        this.operation = operation;
        this.examName = examName;
        factory(examName, args);
    }


    private void factory(String examName, String... args) throws Exception {
        if (ArrayUtils.isNotEmpty(args)) {
            if (args.length >= 1)
                this.propA = args[0];

            if (args.length >= 2)
                this.propB = args[1];
        }

        switch (examName) {
            case "Glucose":
                test = new Glucose(propA);
                break;
            case "Hemoglobin":
                test = new Hemoglobin(propA);
                break;
            case "Sodio":
                test = new Sodio(propA);
                break;
            default:
                throw new Exception("Test not exists");
        }
    }

    public void execute() throws Exception {
        ExamOperation examOperation;
        switch (operation) {
            case "OperationA":
                examOperation = new GenericOperationA(test);
                break;
            case "OperationB":
                examOperation = new GenericOperationB(test);
                break;
            case "Biochemistry":
                if (test instanceof Biochemistry) {
                    examOperation = new BiochemistryOperationA((Biochemistry) test);
                } else {
                    throw new Exception("Operation not exists");
                }
                break;
            default:
                throw new Exception("Operation not exists");
        }
        examOperation.excecute();

    }

    public Test getTest() {
        return test;
    }
}
