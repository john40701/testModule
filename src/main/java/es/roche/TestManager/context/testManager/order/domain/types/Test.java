package es.roche.TestManager.context.testManager.order.domain.types;

public abstract class Test {

    protected String propA;

    public Test(String propA) {
        this.propA = propA;
    }

    public void genericOperationA(){
        System.out.println("Generic Operation A");
    }

    public void genericOperationB(){
        System.out.println("Generic Operation B");
    }

}
