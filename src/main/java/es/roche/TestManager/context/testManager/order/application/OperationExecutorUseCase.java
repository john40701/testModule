package es.roche.TestManager.context.testManager.order.application;

import es.roche.TestManager.context.testManager.order.domain.Command;
import org.springframework.stereotype.Component;

@Component
public class OperationExecutorUseCase {

    public void execute(String examName, String operation, String... args ) throws Exception {
        Command command = new Command(examName, operation, args );
        command.execute();

    }


}
