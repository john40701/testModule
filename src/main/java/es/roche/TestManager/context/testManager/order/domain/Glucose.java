package es.roche.TestManager.context.testManager.order.domain;

import es.roche.TestManager.context.testManager.order.domain.types.Biochemistry;

public class Glucose extends Biochemistry {
    public Glucose(String propA) {
        super(propA);
    }
}
