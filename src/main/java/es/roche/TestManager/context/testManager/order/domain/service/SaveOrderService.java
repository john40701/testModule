package es.roche.TestManager.context.testManager.order.domain.service;

import es.roche.TestManager.context.testManager.order.domain.Command;
import es.roche.TestManager.context.testManager.order.domain.Order;
import es.roche.TestManager.context.testManager.order.domain.dto.TestDTO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class SaveOrderService {

    public Boolean save(String paramA, List<TestDTO> test){
        Order order = new Order(paramA);
        AtomicReference<Boolean> error = new AtomicReference<>(true);
        test.stream().forEach( x -> {
                    try {

                        Command command =new Command(x.name, "Order Creation", x.getParamA(), x.getParamB());
                        order.Add(command.getTest());
                    } catch (Exception e) {
                        //Todo handleErrors
                        error.set(false);
                    }

                }
        );
        if(error.get()){
            System.out.println("Error Saving Test");
        }else{
            System.out.println("Test Saved");
        }

        return error.get();


    }

}
